	//PLEASE PUSH THIS CODE
	//PLEASE PUSH THIS CODE
	//PLEASE PUSH THIS CODE
	//PLEASE PUSH THIS CODE
	//PLEASE PUSH THIS CODE
	//PLEASE PUSH THIS CODE
	//PLEASE PUSH THIS CODE
	//PLEASE PUSH THIS CODE


const express = require('express')
const app = express()
let {people} = require('./data')

// static assets
app.use(express.static('./methods-public'))

//Traditional way = more on utilization of HTML and form to get the data. See index.html in the methods-public folder
//We use the express.urlencoded so that in the http post methods, we can utilize the body method of the req. This middleware helps us parse the incoming data and use the req.body
app.use(express.urlencoded({extended:false}))
//parse json
app.use(express.json())
//Post Method - Post data
//Recipe: for post methods, use the urlencoded, parse the json through req.body, and from there utilize a logic to do what you want with the data

app.post('/api/people',(req,res)=>{
	const {name} = req.body

	if(!name){
		res.status(400).json({success:false, msg:'This will show up since we used the msg object'})
	}
	res.status(201).json({success:true, person:name})

})

app.post('/api/postman/people',(req,res)=>{
	const {name} = req.body

	if(!name){
		res.status(401).json({success: false, msg: 'there is no data'})
	}

	res.status(200).json({success:true, data:[...people, name]})
})

app.post('/login',(req,res)=>{
	//the name here is from the req.body object. Name was the name of the key initiated in the html form
	const {name} = req.body

	if(name){
		return res.status(200).send(`Welcome ${name}`)
	}
	res.status(401).send('Please provide credentials')
})


//Get Method - Read Data
//Recipe: Straightforward, app.get gets all the get request
app.get('/api/people',(req,res)=>{
	console.log('req.method:')
	console.log(req.method)
	//res.json() just returns a json formatted data
	res.status(200).json({success:true, data:people})
})

app.put('/api/people/:id',(req,res)=>{
	const {id} = req.params
	const {name} = req.body

	//this is to check if the people object does have an id within the object
	const person = people.find((person)=>person.id ===Number(id))

	if(!person){
		return res
			.status(404)
			.json({success: false, msg: `no person with id ${id}`})
	}
	const newPeople = people.map((person)=>{
		if(person.id === Number(id)){
			person.name = name
		}
		return person
	})

	res.status(200).json({success:true, data: newPeople})
})

app.delete('/api/people/:id',(req,res)=>{
	const person = people.find((person)=>person.id ===Number(req.params.id))

	if(!person){
		return res
			.status(404)
			.json({success: false, msg: `no person with id ${req.params.id}`})
	}

	const newPeople = people.filter((person)=> person.id!==Number(req.params.id))
	return res.status(200).json({success:true, data: newPeople})
})

app.listen(3000, ()=>{
	console.log('Server is listening at 3000')
})