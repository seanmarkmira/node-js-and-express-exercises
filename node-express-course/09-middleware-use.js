const express = require('express')
const app = express()
const logger = require('./logger')
// req => middleware => res

//If we are going to utilize a middleware, we must either pass it to another middle ware or method or end it with res.send('something')(terminate) 
//app.use logger will invoke or we will use app logger for every routes. This must be before any routes
app.use('/api',logger)
//Interestingly, you can apply app.use to a specific route. For example, if you use 2 arguements, first is the route and the second is the exported function, and use the first arguement as '/api', this means that any req.url that has a base of /api will use the exported module (logger). /api/products, /api/items, and so on.


app.get('/', (req,res)=>{
	res.send('Home')
})

app.get('/about', (req,res)=>{
	res.send('About')
})

app.get('/api/products', (req,res)=>{
	res.send('About')
})

app.get('/api/items', (req,res)=>{
	res.send('About')
})

app.listen(4000,()=>{
	console.log('Server is listening n port 4000.')
})