const express = require('express')
const app = express()
const logger = require('./logger')
const authorize = require('./authorize.js')
const morgan = require('morgan')
// req => middleware => res

//Additionally, we have multiple options to use in middleware:
// 1. use vs route
// 2. options - our own/ express/ third party
//For third party, we can use the morgan npm

//morgan('tiny') is from the morgan package. tiny means that just ran the essentials
app.use(morgan('tiny'))

app.get('/', (req,res)=>{
	res.send('Home')
})

app.get('/about', (req,res)=>{
	res.send('About')
})

app.get('/api/products', (req,res)=>{
	res.send('API PRODUCTS')
})

app.get('/api/items',(req,res)=>{
	console.log('req.user:')
	console.log(req.user)
	res.send('API ITEMS')
})

app.listen(4000,()=>{
	console.log('Server is listening n port 4000.')
})