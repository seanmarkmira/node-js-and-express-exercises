const express = require('express')
const app = express()
const logger = require('./logger')
const authorize = require('./authorize.js')
// req => middleware => res

//app.use logger will invoke or we will use app logger for every routes. This must be before any routes. If you have multiple middleware, you can use an arguement that all of the middlewares are in an array app.use([logger1, logger2])
//You can also use a logger specifically into a single route. See route /api/items
// app.use([logger, authorize])

//Additionally, we have multiple options to use in middleware:
// 1. use vs route
// 2. options - our own/ express/ third party
//For third party, we can use the morgan npm


app.get('/', (req,res)=>{
	res.send('Home')
})

app.get('/about', (req,res)=>{
	res.send('About')
})

app.get('/api/products', (req,res)=>{
	res.send('API PRODUCTS')
})

//[logger, authorize] is the middleware to be used in this route
app.get('/api/items', [logger, authorize],(req,res)=>{
	console.log('req.user:')
	console.log(req.user)
	res.send('API ITEMS')
})

app.listen(4000,()=>{
	console.log('Server is listening n port 4000.')
})