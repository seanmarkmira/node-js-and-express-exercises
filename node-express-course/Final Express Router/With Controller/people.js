const express = require('express')
const router = express.Router();

const {getPeople,
	createPerson,
	deletePerson,
	updatePerson} = require('../Controller/people')

//Flavoring in routes either 2 ways
//First flavor
// router.get('/', getPeople)
// router.post('/', createPerson)
// router.put('/:id',updatePerson)
// router.delete('/:id', deletePerson)

//Second flavor, much more cleaner
router.route('/').get(getPeople).post(createPerson)
router.route('/:id').put(updatePerson).delete(deletePerson)

module.exports = router
