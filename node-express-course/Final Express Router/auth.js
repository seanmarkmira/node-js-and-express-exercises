const express = require('express')
const router = express.Router();

router.post('/',(req,res)=>{
	//the name here is from the req.body object. Name was the name of the key initiated in the html form
	const {name} = req.body

	if(name){
		return res.status(200).send(`Welcome ${name}`)
	}
	res.status(401).send('Please provide credentials')
})

module.exports = router