const express = require('express')
const router = express.Router();
let {people} = require('../data')


router.post('/',(req,res)=>{
	const {name} = req.body

	if(!name){
		res.status(401).json({success: false, msg: 'there is no data'})
	}

	res.status(200).json({success:true, data:[...people, name]})
})


//Get Method - Read Data
//Recipe: Straightforward, app.get gets all the get request
router.get('/',(req,res)=>{
	console.log('req.method:')
	console.log(req.method)
	//res.json() just returns a json formatted data
	res.status(200).json({success:true, data:people})
})

router.put('/:id',(req,res)=>{
	const {id} = req.params
	const {name} = req.body

	//this is to check if the people object does have an id within the object
	const person = people.find((person)=>person.id ===Number(id))

	if(!person){
		return res
			.status(404)
			.json({success: false, msg: `no person with id ${id}`})
	}
	const newPeople = people.map((person)=>{
		if(person.id === Number(id)){
			person.name = name
		}
		return person
	})

	res.status(200).json({success:true, data: newPeople})
})

router.delete('/:id',(req,res)=>{
	const person = people.find((person)=>person.id ===Number(req.params.id))

	if(!person){
		return res
			.status(404)
			.json({success: false, msg: `no person with id ${req.params.id}`})
	}

	const newPeople = people.filter((person)=> person.id!==Number(req.params.id))
	return res.status(200).json({success:true, data: newPeople})
})

module.exports = router
