const {createReadStream} = require('fs');

const stream = createReadStream('../content/subfolder/big.txt', {highWaterMark:90000, encoding:'utf8'})

//the result would give you 64,64,37
//this means that it chunk read the first 64, second 64, and then last 37
stream.on('data', (result)=>{
	console.log(result)
})

stream.on('error',(err) => console.log(err))
//default 64kb
// last buffer - remainder
// highWaterMark - control size - upon testing the highWaterMark control size is all about how much of the size you are going to utilize
// const stream = createReadStream('../content/subfolder/big.txt',{highWaterMark: 90000})
// const stream = createReadStream('../content/subfolder/big.txt',{encoding: 'utf8'})