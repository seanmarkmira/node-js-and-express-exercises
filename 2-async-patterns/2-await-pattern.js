const {readFile, writeFile} = require('fs').promises;

const start = async() =>{
	try{
		console.log('read file 1')
		const first = await readFile('../content/subfolder/first.txt', 'utf8');
		console.log('read file 2')
		const second = await readFile('../content/subfolder/second.txt','utf8');
		console.log('write file 1')
		await writeFile('../content/subfolder/result-mind-grenade.txt',`THIS IS AWESOMELY HARD: ${first} ${second}`)
		console.log('this is first')
		console.log(first)
		console.log('this is second')
		console.log(second)
	} catch(error){
	console.log(error)
	}
}

console.log('start of the start invocation')
start()
console.log('end of the start invocation')


//The code below achieves the top but it is more clearer and easier to understand when we use the native node's approach
// const util = require('util');
// const readFilePromise = util.promisify(readFile)
// const writeFilePromise = util.promisify(writeFile)


// const getText = (path) => {
// 	return new Promise((resolve, reject)=>{
// 		readFile(path, 'utf8', (err,data)=>{
// 			if(err){
// 				reject(err)
// 			} else {
// 				resolve(data)
// 			}
// 		})	
// 	})
// }

// getText('../content/testpath.txt')
// 	.then((result) => console.log(result))
// 	.catch((err) => console.log(err))